phyloskims
================

<div>

[![](https://gricad-gitlab.univ-grenoble-alpes.fr/roubine/docker/badges/main/pipeline.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/eric.coissac/phyloskims/-/commits/main)

pipeline status

</div>

The *phyloskims* docker image provides the software programs used to
analyze the genome skims by the
[PhyloAlps](http://phyloalps.org "PhyloAlps web site") and
[PhyloNorway](http://phylonorway.no "PhyloNorway web site") projects.

- The organelle assembler dedicated to assemble highly repeated
  sequences from genome skim data.

- The organelle annotator dedicated to produce ready to submit EMBL flat
  files from assembled sequences

\[\[*TOC*\]\]

## Installing the *phyloskim* scripts

``` bash
curl -L https://gricad-gitlab.univ-grenoble-alpes.fr/eric.coissac/phyloskims/-/raw/main/install-phyloskims.sh \
  | bash
```

### Options of the install script

- **-i** \| **–install-dir**

  Directory where phyloskims scripts are installed (as example use
  /usr/local not /usr/local/bin).

- **-p** \| **–phyloskims-prefix**

  Prefix added to the obitools command names if you want to have several
  versions of obitools at the same time on your system (as example -p g
  will produce goa command instead of doa).

- **-h** \| **–help**

  Display this help message.

``` bash
curl -L https://gricad-gitlab.univ-grenoble-alpes.fr/eric.coissac/phyloskims/-/raw/main/install-phyloskims.sh \
  | bash -s -- --install-dir test_install --phyloskims-prefix k
```

## Organelle assembler

### Example of usage

#### Downloading some data

Here genome skims of *Dryas Octopetala* corresponding to the voucher
*TROM_V\_963983* are downloaded from EBI-SRA database using the `wget`
command.

``` bash
wget http://ftp.sra.ebi.ac.uk/vol1/fastq/ERR555/007/ERR5554787/ERR5554787_1.fastq.gz
wget http://ftp.sra.ebi.ac.uk/vol1/fastq/ERR555/007/ERR5554787/ERR5554787_2.fastq.gz
```

It is also possible to download the same data using the `curl` command.

``` bash
curl http://ftp.sra.ebi.ac.uk/vol1/fastq/ERR555/007/ERR5554787/ERR5554787_1.fastq.gz > ERR5554787_1.fastq.gz
curl http://ftp.sra.ebi.ac.uk/vol1/fastq/ERR555/007/ERR5554787/ERR5554787_2.fastq.gz > ERR5554787_2.fastq.gz
```

#### Indexing the reads

Before being able to assemble some sequences, the organelle assembler
requires for indexing the reads.

``` bash
doa index --estimate-length 0.9 \
    ERR5554787 \
    ERR5554787_1.fastq.gz ERR5554787_2.fastq.gz
```

The organelle assembler algorithm requires that all the indexed reads
must have the same length. Several options of the `index` sub-command
allow for controlling how raw reads are processed before being indexed.
Among them three are controlling the length of the indexed reads.

- **–length** <INDEX:LENGTH>

  The length of the read to index (default indexed length is estimated
  from the first read)

- **–estimate-length** <FRACTION>

  Estimate the length to index for conserving FRACTION of the overall
  data set

- **–minimum-length** <INDEX:MINLENGTH>

  The minimum length of the read to index if the **–estimate-length** is
  activated (default 81)

After indexation the `list` sub command provides information about the
indexed reads.

``` bash
doa list ERR5554787
```

    6618616 97

The command returns two numbers: the count of indexed reads (*e.g.*
\$$6618616$ reads) and the length of indexing (*e.g.* $97$ bp). The
length of indexing must be odd.

#### Assembling the chloroplast genome

``` bash
doa seeds --seeds protChloroArabidopsis ERR5554787 ERR5554787_chloro
```

``` bash
doa buildgraph ERR5554787 ERR5554787_chloro
```

<figure>
<img src="figures/chloro_assembling.svg" style="width:70.0%"
alt="Assembly graph. Each edge corresponds to a fragment of sequence. More green is the edge more it is containing reads matching the probe sequence. The edge labels follow the pattern ID: 5’seq (length) 3’seq [sequencing depth]. IDs can be positive or negative. Two IDs having the same absolute value (e.g. -3/3) corresponds to the same sequence but in reverse/complement orientation. Width of the edges are relative to their sequencing depth." />
<figcaption aria-hidden="true">Assembly graph. Each edge corresponds to
a fragment of sequence. More green is the edge more it is containing
reads matching the probe sequence. The edge labels follow the pattern
ID: 5’seq (length) 3’seq [sequencing depth]. IDs can be positive or
negative. Two IDs having the same absolute value (e.g. -3/3) corresponds
to the same sequence but in reverse/complement orientation. Width of the
edges are relative to their sequencing depth.</figcaption>
</figure>

``` bash
doa unfold ERR5554787 ERR5554787_chloro > ERR5554787_chloro.raw.fasta
```

    [INFO ]  Both segments -15 and 25 are connected (paired-end=74 frg length=275.081081 sd=85.148341)
    [INFO ]  Both segments 25 and 13 are connected (paired-end=34 frg length=284.705882 sd=95.096611)
    [INFO ]  Both segments 13 and -14 are disconnected
    [INFO ]     But linked by 24 pair ended links (gap length=4.000000 sd=117.000000)
    [INFO ]  Both segments -14 and -25 are connected (paired-end=58 frg length=300.155172 sd=103.552818)
    [INFO ]  Path is circular and connected by 109  (length: 287, sd: 93)

``` bash
head ERR5554787_chloro.raw.fasta
```

    >ERR5554787_hq_chloro_1 seq_length=158276; coverage= 72.0; circular=True;  -15 : GAAAA->(18558)->TCTCT  [82] @ {'ERR5554787_hq_chloro_1': [1]}.{connection: 74 - length: 275, sd: 85}.25 : TTAAA->(26318)->TTGGT  [144] @ {'ERR5554787_hq_chloro_1': [2]}.{connection: 34 - length: 284, sd: 95}.13 : TTATA->(6175)->AAAAA  [72] @ {'ERR5554787_hq_chloro_1': [3]}.{N-connection: 24 - Gap length: 4, sd: 117}.-14 : GGATG->(80806)->GAACG  [72] @ {'ERR5554787_hq_chloro_1': [5]}.{connection: 58 - length: 300, sd: 103}.-25 : TGTCA->(26318)->TTAAA  [144] @ {'ERR5554787_hq_chloro_1': [6]}.{connection: 109 - length: 287, sd: 93}
    GAAAAAAAGGATCTCTTCGGGTTTGAAAAACCGATTGTGACTATTCTTTTCGACTATAAA
    AGATGGAATCGTCCGTTGCGGTATATAAAAAACAATCAATTTGAAAATGCTGTAAGAAAT
    GAAATGTCACAATACTTTTTTTATACATGTCAAAGCGATGGAAAAGAAAAAATATCTTTT
    ACGTACCCTCCCAGTTTGGGAACTTTTTTGGAAATGATACAAAGAAAGATGTCTCTGTTC
    ACAAAAGAAAAATTCCCCTCTAATGAGTTTTATAATCATTGGAGTTATACTAATGAACAT
    AAAAAAAAAAACCTAAGCAAAAACTTTATAAATAGAGTAAAAGCTCTCGACAAAACTCTA
    AATGAGGAATCCCTTGTTCTGAATGTACTCGAAAAACGAACTAGATTGTGTAATGATAAG
    ACTAAAAAAGAATACTTACCCCAAATATACGATCCTTTCTTGAATGGACCCTATCGCGTA
    CGAATCAATTTTTTTTTTTCACTCTTAATCATAAATGAAAATTCTATAAAAAATTACATA

    CTTTACTTTTGATTTAATCGAAAGAGTTTTACCAATTCCAACAAATTTTACTTTTAAATT
    TTCTTGAATTGGATCCTTTGGATTTATATATCGAAAATATACTTACGAAGTTGTTCCAAT
    TTATTGATTGATACTAACCTTAGATTCTTGCCCCTGAGAAATGAATCAATACTTTCTACT
    CGAGCTCCATCGTGTACTATTTACATCACCCCCCCCCCCCCAAAAAAAAAANNNNccccc
    caaaaaaaaagagggttccagtgtaacagaacaaatgatgtcgagccaagagcactttca
    ttcctatataattatatatataaaaaaatggtGGATGTAAGAATCCACAACTGATTGTGT
    CCTTCAAGTCGCACGTTGCTTTCTACCACATCGTTTTAAACGAAGTTTTACCATAACATT

    CTTTACTTTTGATTTAATCGAAAGAGTTTTACCAATTCCAACAAATTTTACTTTTAAATT
    TTCTTGAATTGGATCCTTTGGATTTATATATCGAAAATATACTTACGAAGTTGTTCCAAT
    TTATTGATTGATACTAACCTTAGATTCTTGCCCCTGAGAAATGAATCAATACTTTCTACT
    CGAGCTCCATCGTGTACTATTTACATCACCCCCCCCCCCCC...................
    .aaaaaaaaagagggttccagtgtaacagaacaaatgatgtcgagccaagagcactttca
    ttcctatataattatatatataaaaaaatggtGGATGTAAGAATCCACAACTGATTGTGT
    CCTTCAAGTCGCACGTTGCTTTCTACCACATCGTTTTAAACGAAGTTTTACCATAACATT

*Using BLAST at the NCBI Web site, the corrected sequence can be
correctly aligne with RefSeq entry* NC_073102.1, corresponding to the
*Dryas octopetala var. asiatica* chloroplast complete genome (Length:
158271\~bp).

    Query  1     CTTTACTTTTGATTTAATCGAAAGAGTTTTACCAATTCCAACAAATTTTACTTTTAAATT  60
                 |||||||||||||||||||||||||||||||||||||||||||||||||||||| |||||
    Sbjct  5901  CTTTACTTTTGATTTAATCGAAAGAGTTTTACCAATTCCAACAAATTTTACTTTAAAATT  5960

    Query  61    TTCTTGAATTGGATCCTTTGGATTTATATATCGAAAATATACTTACGAAGTTGTTCCAAT  120
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    Sbjct  5961  TTCTTGAATTGGATCCTTTGGATTTATATATCGAAAATATACTTACGAAGTTGTTCCAAT  6020

    Query  121   TTATTGATTGATACTAACCTTAGATTCTTGCCCCTGAGAAATGAATCAATACTTTCTACT  180
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    Sbjct  6021  TTATTGATTGATACTAACCTTAGATTCTTGCCCCTGAGAAATGAATCAATACTTTCTACT  6080

    Query  181   CGAGCTCCATCGTGTACTATTTACATCAcccccccccccccaaaaaaaaaGAGGGTTCCA  240
                 ||||||||||||||||||||||||||||||||||||||   |||||||||||||||||||
    Sbjct  6081  CGAGCTCCATCGTGTACTATTTACATCACCCCCCCCCCAAAAAAAAAAAAGAGGGTTCCA  6140

    Query  241   GTGTAACAGAACAAATGATGTCGAGCCAAGAGCACTTTCATTCCTATATAATTATATATA  300
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    Sbjct  6141  GTGTAACAGAACAAATGATGTCGAGCCAAGAGCACTTTCATTCCTATATAATTATATATA  6200

    Query  301   TaaaaaaaTGGTGGATGTAAGAATCCACAACTGATTGTGTCCTTCAAGTCGCACGTTGCT  360
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    Sbjct  6201  TAAAAAAATGGTGGATGTAAGAATCCACAACTGATTGTGTCCTTCAAGTCGCACGTTGCT  6260

    Query  361   TTCTACCACATCGTTTTAAACGAAGTTTTACCATAACATT  400
                 ||||||||||||||||||||||||||||||||||||||||
    Sbjct  6261  TTCTACCACATCGTTTTAAACGAAGTTTTACCATAACATT  6300

BLAST alignment of the edited region with *RefSeq entry*
[NC_073102.1](https://www.ncbi.nlm.nih.gov/nucleotide/NC_073102.1). The
edited region at position 178 of the query match perfectly with the
already published genome. Only a polymorphism at the level of the
microsatellite is observable. Similar alignments are observable with
already published genomes of *Dryas octopetala* and of the genus
*Dryas*.

#### Assembling the nuclear rDNA cluster

``` bash
doa seeds --seeds nucrRNAArabidopsis ERR5554787 ERR5554787_rrnanuc
```

``` bash
doa buildgraph ERR5554787 ERR5554787_rrnanuc
```

``` bash
doa unfoldrdna ERR5554787 ERR5554787_rrnanuc > ERR5554787_rrnanuc.raw.fasta
```

``` bash
head ERR5554787_rrnanuc.raw.fasta
```

    >ERR5554787_rrnanuc_1 seq_length=6895; coverage=1500.0; circular=False;  -70 : GACGGTGG->(8)  [5891] @ {'ERR5554787_rrnanuc_1': [1]}.{connection: 45 - length: 120, sd: 9}.-9 : CGGTC->(30)->CGTCC  [4059] @ {'ERR5554787_rrnanuc_1': [2]}.{connection: 108 - length: 235, sd: 95}.-13 : TCCCA->(6756)->CTAGA  [1500] @ {'ERR5554787_rrnanuc_1': [3]}
    cccacagtgtgtggatagttgttgctcggattgcctagtcgtgcttgtgcgtgcaatgga
    gtttgtcgaccttcggggcctgtcattgcgcgtggaatgtcGACGGTGGCGGTCTTGGCA
    TCGATGATTCCACTCGTCCTCCCAAACACATCCCCCACAAAACACCTCTCCCCTCCACAA
    AACACCTTTCCCCTCCATGGTTGCCTTTGGAATGTTGCCTTCGGATTGTTGCCTTCAGAT
    TGTTGCCTTCGGATTGTTGCCTTCGGATTGTTGCCTTCGGAATGGTGGATGAGCGGCGAA
    TTGGACTGCCTATGCTTGCTTGTGCCGAGCCCTTGCGGGCAATGCACAAGCATGTCGAGG
    ATGGCACCCCGAGGAGCTGCGTTGACATGCCACAATATTGTGTTCTTTGTTTTCATGCCT
    AGCGATGCCGGCTTGGCTCCACTCGGTGTGCCGCATGTGTAGCTATCCTTCGGGCTAAGT
    TAATTTGCGGTTCGTTTGGTGGTGCTGGGCTTGGCTAACGTGACGGTGTGTGAGTGGTGA

### global options

- **-h** \| **–help**

  show this help message and exit

- **–version**

  Print the version of The Organelle Assembler

- **–log** <LOGFILENAME>

  Create a logfile

- **–no-progress**

  Do not print the progress bar during analyzes

### sub commands

- **buildgraph** Build the initial assembling graph
- **clone** copy an assembly
- **compact** Recompact the assembling graph
- **cutlow** Cut low coverage edge in an assembling graph
- **graph** Build a graph file from the assembling graph
- **graphstats** Print some statistics about the assembling graph
- **index** Index a set of reads
- **list** List information about a read index
- **restore** Restore the intermediate graph saved regularly during the
  assembling process.
- **seeds** Build the set of seed reads
- **unfold** Universal assembling graph unfolder
- **unfoldrdna** Assembling graph unfolder for the nuclear rDNA complex

## Organelle annotator

The organelle annotator is converting the FASTA file generated by the
assembler to an annotated EMBL flat-file ready to be submitted to EBI.
The dorgannot annotates :

- the inverted repeats using repseek (Achaz et al. 2007)

- the large single copy (LSC) and small single copy (SSC) regions

- the tRNA (using aragorn, ) and differentiate among the three type of
  CAU tRNA (met, fmet and leu)

- the rRNA

``` bash
dorgannot -c \
      -c -t 57948 -L TV963983 \
      -s TROM_V_963983 -P PRJEB43458 \
      --list-file TROM_V_963983.chloro.chrlist  \
      ERR5554787_chloro.edited.fasta \
      > TROM_V_963983.chloro.embl
```

### Options:

Defining the sequence category

- **-c** \| **–chloroplast**

  Selects for the annotation of a chloroplast genome This is the default
  mode

- **-r** \| **–nuclear-rdna**

  Selects for the annotation of the rDNA nuclear cistron

Providing information about the sequence

- **-s** \| **–specimen** \###

  Represents the specimen voucher identifier (*e.g.* for herbarium
  sample TROM_V\_991090 will be added to the /specimen_voucher qualifier

- **-t** \| **–ncbi-taxid** \###

  Represents the ncbi taxid associated to the sequence

- **-o** \| **–organism** <organism_name>

  Allows for specifiying the organism name in the embl generated file
  Spaces have to be substituted by underscore ex : Abies_alba

- **-b** \| **–country** “<country_value>\[:<region>\]\[, <locality>\]”

  Location (at least country) of collection

- **-f** \| **–not-force-ncbi**

  Do not force the name of the organism to match the NCBI scientific
  name. if the provided NCB taxid is publically defined

Information related to an ENA project

- -P \| **–project** <ENA Project>

  The accession number of the related project in ENA

- -i \| **–id-prefix** <prefix>

  prefix used to build the sequence identifier the number of the contig
  is append to the prefix to build the complete id. This id is used only
  if an ENA project is specified.

- -L \| **–locus-prefix** <prefix>

  Prefix used to build the locus tag of every annotated genes generated
  locus tags follow the pattern : prefix\_###, where \### is a number
  following the order of gene in the EMBL flat file starting at locus
  tag shift (default 1).

- **-S** \| **–locus-shift** \<###\>

  Start number for building locus tags

- **–list-file** <FILENAME>

  The chromosome list file name to file for ENA submission

Annotation of partial sequences

- **-p** \| **–partial**

  Indicates that the genome sequence is partial and therefore in several
  contigs

- **-l** \| **–min-length**

  Indicates for partial mode the minimum length of contig to annotate

Setting up the sequence annotation

- **-N** \| **–no-normalization**

  Does not normalize the sequence before annotation

- **-I** \| **–no-ir-detection**

  Does not look for inverted repeats in the plastid genome

- **-C** \| **–no-cds**

  Do not annotate CDSs

- **-D** \| **–no-cds-pass1**

  Do not annotate core CDSs

- **-E** \| **–no-cds-pass2**

  Do not annotate rps12 CDS

- **-F** \| **–no-cds-pass3**

  Do not annotate shell and dust CDSs

- **-T** \| **–no-trna**

  Do not look for transfer RNA

- **-R** \| **–no-rrna**

  Do not look for ribosomal RNA

# Submission to EBI-ENA

The manifiest file `TROM_V_963983.chloro.manifest` has to be created to
link the assembly to a sample in the EBI database

    STUDY           PRJEB43458
    SAMPLE          SAMEA8202390
    RUN_REF         ERR5554787
    ASSEMBLYNAME    BXA_BVB_1
    ASSEMBLY_TYPE   isolate
    COVERAGE        65
    PROGRAM         Organelle Assembler (https://metabarcoding.org/asm)
    PLATFORM        ILLUMINA
    MINGAPLENGTH    5
    MOLECULETYPE    genomic DNA
    FLATFILE        TROM_V_963983.chloro.embl.gz
    CHROMOSOME_LIST TROM_V_963983.chloro.chrlist.gz

<div id="refs" class="references csl-bib-body hanging-indent">

<div id="ref-Achaz2007-sm" class="csl-entry">

Achaz, Guillaume, Frédéric Boyer, Eduardo P C Rocha, Alain Viari, and
Eric Coissac. 2007. “<span class="nocase">Repseek, a tool to retrieve
approximate repeats from large DNA sequences</span>.” *Bioinformatics*
23 (1): 119–21. <https://doi.org/10.1093/bioinformatics/btl519>.

</div>

</div>
